<?php

include ('inc/header.php'); ?>

<div class="container">
    <p>Quand la bise fut venue.</p>
    <p>Pas un seul petit morceau</p>
    <p>De mouche ou de vermisseau.</p>
    <p>Chez la Fourmi sa voisine,</p>
    <p>La priant de lui prêter</p>
    <p>Quelque grain pour subsister</p>
    <p>Je vous paierai, lui dit-elle,</p>
    <p>Avant l'août (2), foi d'animal,</p>
    <p>Intérêt et principal.</p>
    <p>C'est là son moindre défaut (3).</p>
    <p>Que faisiez-vous au temps chaud ?</p>
    <p>Dit-elle à cette emprunteuse.</p>
    <p>Vous chantiez ? j'en suis fort aise :</p>
    <p>Je chantais, ne vous déplaise.</p>
    <p>Eh bien! dansez maintenant.</p>
    <p>La Fourmi n'est pas prêteuse ;</p>
    <p>C'est là son moindre défaut (3).</p>
    <p>Que faisiez-vous au temps chaud ?</p>
    <p>Dit-elle à cette emprunteuse (4).</p>
    <p>Nuit et jour à tout venant</p>
    <p>Je chantais, ne vous déplaise.</p>
    <p>Vous chantiez ? j'en suis fort aise :</p>
    <p>Et bien ! dansez maintenant.</p>
    <p>yoyoyo</p>
</div>

<?php
include ('inc/footer.php');
